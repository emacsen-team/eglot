eglot (1.15-4) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Set upstream metadata fields: Repository.

 -- Xiyue Deng <manphiz@gmail.com>  Tue, 11 Oct 2022 01:20:49 +0200

eglot (1.15-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:01:35 +0900

eglot (1.15-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 24 Jul 2024 21:58:50 +0900

eglot (1.15-1) unstable; urgency=medium

  [ Aymeric Agon-Rambosson ]
  * New 1.15 upstream release (Closes: #1053976).
  * Add explicit emacs recommends to elpa-eglot.
  * Drop now unnecessary d/p/correct-version-number.patch.
  * Bump Standards-Version to 4.6.2 (no changes required).
  * Add language servers and elisp libs as Build-Depends for unit tests :
    - ccls
    - clangd
    - elpa-yasnippet (to test snippet expansion)
    - elpa-company (to test snippet expansion with company)
  * Remove d/install (referred file does not exist anymore).
  * Remove d/docs (referred file does not exist anymore).

  [ Debian Janitor ]
  * Trim trailing whitespace in d/rules.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Wed, 18 Oct 2023 21:29:59 +0200

eglot (1.9-2) unstable; urgency=medium

  * Source-only upload.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 16 Oct 2022 12:52:52 -0700

eglot (1.9-1) unstable; urgency=medium

  * Initial release (Closes: #1021462).

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Sun, 09 Oct 2022 02:52:04 +0200
